import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpService } from '../../providers/http-service';
import { MediaObject, Media } from '@ionic-native/media';
import { MusicControls } from '@ionic-native/music-controls';
import { HeaderColor } from '@ionic-native/header-color';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@IonicPage()
@Component({
	selector: 'page-radio',
	templateUrl: 'radio.html',
})
export class RadioPage {
	bg_color: string;
	color: string;
	radio_name: string;
	logo: string;
	url: string;
	media_object: MediaObject;
	status;

	constructor(public navCtrl: NavController, private header: HeaderColor, private statusBar: StatusBar, private splash: SplashScreen,
		private musicControls: MusicControls, private media: Media, public navParams: NavParams, private http: HttpService, private toast: ToastController) {
	}

	ionViewDidLoad() {
		this.loadInfo();
	}

	playStopRadio() {
		console.log('playStopRadio', this.status);
		if (this.status != 'loading') {
			if (this.status == 'playing') {
				this.media_object.setVolume(0)
				this.status = 'paused';
			} else if (this.status == 'paused') {
				this.media_object.setVolume(1);
				this.status = 'playing';
			} else {
				this.loadMedia(this.url);
				this.status = 'loading';
			}
		} else {
			this.toast.create({ position: 'middle', duration: 5000, closeButtonText: 'Ok', showCloseButton: true, message: 'Aguarde o carregamento da rádio' }).present();
		}
	}

	private loadInfo() {
		this.http.getInfo().subscribe(data => {
			this.bg_color = data.background;
			this.radio_name = data.name;
			this.color = data.color;
			this.logo = data.logo;
			this.url = `http://${data.server}:${data.port}/${data.mount}`;
			this.header.tint(data.background);
			this.statusBar.backgroundColorByHexString(data.background);
			this.splash.hide();
		});
	}

	private loadMedia(url) {
		this.media_object = this.media.create(url);
		this.media_object.play();
		let _self = this;
		this.media_object.onStatusUpdate.subscribe(status_radio => {
			console.log(status_radio, status_radio == 2, status_radio != 1);
			if (status_radio == 2) {
				this.status = 'paused';
				this.playStopRadio();
				console.log('playing');
			} else if (status_radio != 1) {
				_self.status = 'paused';
				console.log('paused');
				//this.musicControls.updateIsPlaying(false);
			}
		});
		this.media_object.onError.subscribe(error => {
			console.log('Error!', error);
			this.status = 'paused';
			this.musicControls.updateIsPlaying(false);
		});
	}

	private musicControl() {
		let _self = this;
		this.musicControls.create({
			artist: this.radio_name,
			track: '',
			cover: this.logo,
			isPlaying: this.status == 's',
			dismissable: false,
			hasPrev: false,
			hasNext: false,
			hasClose: true,
			ticker: 'Carregando...',
			playIcon: 'ic_play',
			pauseIcon: 'ic_stop',
			closeIcon: 'ic_destroy',
			notificationIcon: 'ic_notif'
		});
		this.musicControls.subscribe().subscribe((action) => {
			this.musicControlAction(action, _self);
		});
		this.musicControls.listen();
	}

	private musicControlAction(action, _self) {
		const message = JSON.parse(action).message;
		switch (message) {
			case 'music-controls-pause':
				console.log('music-controls-pause');
				_self.stop();
				_self.musicControls.updateIsPlaying(false);
				break;
			case 'music-controls-play':
				console.log('music-controls-play');
				_self.stats = null;
				_self.musicControl();
				_self.play();
				_self.musicControls.updateIsPlaying(true);
				break;
			case 'music-controls-destroy':
				_self.stopAndClose();
				_self.musicControls.destroy().then(data => {
					console.log(data);
				}).catch(error => {
					console.log(error);
				})
				console.log('music-controls-destroy');
				break;
			case 'music-controls-media-button':
				console.log('music-controls-media-button');
				break;
			default:
				break;
		}
	}

}
