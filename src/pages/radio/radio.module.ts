import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RadioPage } from './radio';
import { HttpService } from '../../providers/http-service';
import { Media } from '@ionic-native/media';
import { MusicControls } from '@ionic-native/music-controls';
import { StatusBar } from '@ionic-native/status-bar';
import { HeaderColor } from '@ionic-native/header-color';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
	declarations: [RadioPage],
	imports: [IonicPageModule.forChild(RadioPage)],
	providers: [HttpService, Media, MusicControls, StatusBar, SplashScreen, HeaderColor]
})
export class RadioPageModule { }
