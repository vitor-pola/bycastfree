import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {

	public API_PATH: string = 'https://api.app-gratis.bycast.com.br/app/token/c4ca4238a0b923820dcc509a6f75849b';

	constructor(private http: Http) { }

	getInfo() {
		return this.http.get(this.API_PATH, {}).map(this.extractData);
	}

	private extractData(res: Response) {
		let body = res.json();
		console.log(body);
		return body || {};
	}
}
